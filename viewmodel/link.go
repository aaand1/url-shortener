package viewmodel

import (
	"bluefield/url-shortener/model"
	"encoding/json"
	"net/url"
)

type IShortLinkGenerator interface {
	GenerateShortUrl(*model.Link) (*url.URL, error)
}

type CreateUrlData struct {
	TargetUrl string `json:"targetUrl"`
	Name      string `json:"name"`
}

type LinkResponseViewModel struct {
	Hash      string `json:"hash"`
	TargetUrl string `json:"targetUrl"`
	ShortUrl  string `json:"shortUrl"`
	Name      string `json:"name"`
	CreatedAt int64  `json:"createdAt"`
	UpdatedAt int64  `json:"updatedAt"`
}

func CreateUrlViewModel(inputJson []byte) (CreateUrlData, error) {
	var vm CreateUrlData
	err := json.Unmarshal(inputJson, &vm)
	if err != nil {
		return vm, err
	}

	return vm, nil
}

func LinksToVMs(from *[]model.Link, generator IShortLinkGenerator) *[]LinkResponseViewModel {
	to := make([]LinkResponseViewModel, len(*from))

	for i, e := range *from {
		to[i] = *LinkToVM(&e, generator)
	}

	return &to
}

func LinkToVM(linkModel *model.Link, generator IShortLinkGenerator) *LinkResponseViewModel {
	shortUrl, _ := generator.GenerateShortUrl(linkModel)
	return &LinkResponseViewModel{
		Hash:      linkModel.Hash,
		TargetUrl: linkModel.TargetUrl.String(),
		ShortUrl:  (*shortUrl).String(),
		Name:      linkModel.Name,
		CreatedAt: linkModel.CreatedAt.UnixNano(),
		UpdatedAt: linkModel.UpdatedAt.UnixNano(),
	}
}

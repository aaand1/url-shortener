//+build unit

package main

import (
	"github.com/stretchr/testify/assert"
	"os"
	"os/signal"
	"syscall"
	"testing"
)

func Test_listenShutdownSignal(t *testing.T) {
	type args struct {
		inputChannel   chan os.Signal
		shutdownSignal os.Signal
	}

	tests := []struct {
		name     string
		args     args
		isCalled bool
	}{
		{"It should call shutdown on SIGINT signal",
			args{
				make(chan os.Signal, 1),
				os.Interrupt,
			},
			true,
		}, {"It should call shutdown on SIGKILL signal",
			args{
				make(chan os.Signal, 1),
				os.Kill,
			},
			true,
		}, {"It shouldn't call shutdown on non SIGINT, SIGKILL signals",
			args{
				make(chan os.Signal, 1),
				syscall.SIGHUP,
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var called = false
			var calledSignal os.Signal = nil
			onShutdown := func(inputSignal os.Signal) {
				called = true
				calledSignal = inputSignal
			}
			go func() {
				tt.args.inputChannel <- tt.args.shutdownSignal
			}()

			listenShutdownSignal(tt.args.inputChannel, onShutdown)
			signal.Stop(tt.args.inputChannel)
			close(tt.args.inputChannel)

			assert.Equal(t, called, tt.isCalled,
				"expected tt.isCalled to be %v, but got %v", tt.isCalled, called)
			if tt.isCalled {
				assert.Equal(t, calledSignal, tt.args.shutdownSignal,
					"expected callback to be called with %v, but got %v", tt.args.shutdownSignal, calledSignal)
			}
		})
	}
}

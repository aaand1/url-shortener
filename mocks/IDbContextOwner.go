// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import db "bluefield/url-shortener/db"
import mock "github.com/stretchr/testify/mock"

// IDbContextOwner is an autogenerated mock type for the IDbContextOwner type
type IDbContextOwner struct {
	mock.Mock
}

// DbContext provides a mock function with given fields:
func (_m *IDbContextOwner) DbContext() db.IDbContext {
	ret := _m.Called()

	var r0 db.IDbContext
	if rf, ok := ret.Get(0).(func() db.IDbContext); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(db.IDbContext)
		}
	}

	return r0
}

// SetDbContext provides a mock function with given fields: ctx
func (_m *IDbContextOwner) SetDbContext(ctx db.IDbContext) {
	_m.Called(ctx)
}

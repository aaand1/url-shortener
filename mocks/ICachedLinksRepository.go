// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import db "bluefield/url-shortener/db"
import mock "github.com/stretchr/testify/mock"
import model "bluefield/url-shortener/model"

import url "net/url"

// ICachedLinksRepository is an autogenerated mock type for the ICachedLinksRepository type
type ICachedLinksRepository struct {
	mock.Mock
}

// CreateLink provides a mock function with given fields: _a0, _a1
func (_m *ICachedLinksRepository) CreateLink(_a0 *url.URL, _a1 string) (*model.Link, bool, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *model.Link
	if rf, ok := ret.Get(0).(func(*url.URL, string) *model.Link); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*model.Link)
		}
	}

	var r1 bool
	if rf, ok := ret.Get(1).(func(*url.URL, string) bool); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Get(1).(bool)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(*url.URL, string) error); ok {
		r2 = rf(_a0, _a1)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// DbContext provides a mock function with given fields:
func (_m *ICachedLinksRepository) DbContext() db.IDbContext {
	ret := _m.Called()

	var r0 db.IDbContext
	if rf, ok := ret.Get(0).(func() db.IDbContext); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(db.IDbContext)
		}
	}

	return r0
}

// DeleteLink provides a mock function with given fields: _a0
func (_m *ICachedLinksRepository) DeleteLink(_a0 string) error {
	ret := _m.Called(_a0)

	var r0 error
	if rf, ok := ret.Get(0).(func(string) error); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteUrl provides a mock function with given fields: hash
func (_m *ICachedLinksRepository) DeleteUrl(hash string) {
	_m.Called(hash)
}

// GetLinks provides a mock function with given fields: nameQuery, createdBy, limit, offset
func (_m *ICachedLinksRepository) GetLinks(nameQuery string, createdBy string, limit uint, offset uint) (*[]model.Link, error) {
	ret := _m.Called(nameQuery, createdBy, limit, offset)

	var r0 *[]model.Link
	if rf, ok := ret.Get(0).(func(string, string, uint, uint) *[]model.Link); ok {
		r0 = rf(nameQuery, createdBy, limit, offset)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*[]model.Link)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, string, uint, uint) error); ok {
		r1 = rf(nameQuery, createdBy, limit, offset)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTargetUrlByHash provides a mock function with given fields: _a0
func (_m *ICachedLinksRepository) GetTargetUrlByHash(_a0 string) (string, error) {
	ret := _m.Called(_a0)

	var r0 string
	if rf, ok := ret.Get(0).(func(string) string); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUrl provides a mock function with given fields: hash
func (_m *ICachedLinksRepository) GetUrl(hash string) (string, bool) {
	ret := _m.Called(hash)

	var r0 string
	if rf, ok := ret.Get(0).(func(string) string); ok {
		r0 = rf(hash)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 bool
	if rf, ok := ret.Get(1).(func(string) bool); ok {
		r1 = rf(hash)
	} else {
		r1 = ret.Get(1).(bool)
	}

	return r0, r1
}

// SetDbContext provides a mock function with given fields: ctx
func (_m *ICachedLinksRepository) SetDbContext(ctx db.IDbContext) {
	_m.Called(ctx)
}

// SetUrl provides a mock function with given fields: hash, targetUrl
func (_m *ICachedLinksRepository) SetUrl(hash string, targetUrl string) {
	_m.Called(hash, targetUrl)
}

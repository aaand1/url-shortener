package model

import (
	"crypto/md5"
	"fmt"
	"net/url"
	"time"
)

type Link struct {
	Name      string
	TargetUrl url.URL
	Hash      string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func NewLink(targetUrl *url.URL, name string, createdAt, updatedAt *time.Time) *Link {
	var now = time.Now().UTC()
	if createdAt == nil {
		createdAt = &now
	}
	if updatedAt == nil {
		updatedAt = &now
	}
	updatedAt.In(time.UTC)
	createdAt.In(time.UTC)
	return &Link{
		name, *targetUrl, fmt.Sprintf("%x", md5.Sum([]byte(targetUrl.String()))),
		*createdAt, *updatedAt}
}

package manager

import (
	"bluefield/url-shortener/config"
	"bluefield/url-shortener/db"
	"bluefield/url-shortener/model"
	"fmt"
	"net/url"
)

type IUrlShortenerManager interface {
	GenerateShortUrl(*model.Link) (*url.URL, error)
	db.IDbContextOwner
}

type UrlShortenerManager struct {
	dbContext db.IDbContext
}

func (manager *UrlShortenerManager) DbContext() db.IDbContext {
	return manager.dbContext
}

func (manager *UrlShortenerManager) SetDbContext(ctx db.IDbContext) {
	manager.dbContext = ctx
}

func (manager *UrlShortenerManager) GenerateShortUrl(linkModel *model.Link) (*url.URL, error) {
	basePath := config.Get().RedirectUrlBasePath
	shortLink, err := url.Parse(fmt.Sprintf("%v/%s", basePath.String(), linkModel.Hash))
	if err != nil {
		return nil, err
	}
	return shortLink, nil
}

//+build !integration

package controller

import (
	"bluefield/url-shortener/config"
	"bluefield/url-shortener/manager"
	"bluefield/url-shortener/mocks"
	"bluefield/url-shortener/model"
	"bluefield/url-shortener/viewmodel"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"

	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestMain(m *testing.M) {
	os.Setenv("URL_SHORTENER_REDIRECTURLBASEPATH", "https://test.localhost:8080/api/")
	os.Setenv("URL_SHORTENER_CONNECTIONSTRING", "./url_shortener_test.db")
	if _, err := config.Init(); err != nil {
		panic(err)
	}
	m.Run()
}

func newErrorableReader(mockData []byte, expectedReadError error) io.Reader {
	return &errorableReader{bytes.NewReader(mockData), expectedReadError}
}

type errorableReader struct {
	*bytes.Reader
	readError error
}

func (reader *errorableReader) Read(p []byte) (n int, err error) {
	return 0, reader.readError
}

func TestUrlsController_handleCreate(t *testing.T) {
	cases := []struct {
		name      string
		targetUrl string
		linkName  string
		isNew     bool
		inputBody io.Reader

		expectedResponseCode int
		expectedErrorCode    string
	}{
		{
			"It should validate input body, create db record and return generated url in viewmodel",
			"http://www.google.com", "google shortcut", true,
			bytes.NewReader([]byte(`{"targetUrl": "http://www.google.com", "name": "google shortcut"}`)),
			http.StatusCreated, "",
		},
		{
			"It shouldn't fail without json body's name field specified",
			"http://www.google.com", "google shortcut", true,
			bytes.NewReader([]byte(`{"targetUrl": "http://www.google.com"}`)),
			http.StatusCreated, "",
		},
		{
			"It shouldn't fail with json body's empty name field",
			"http://www.google.com", "", true,
			bytes.NewReader([]byte(`{"targetUrl": "http://www.google.com", "name": ""}`)),
			http.StatusCreated, "",
		},
		{
			"It should respond with http/200 for already existing link creation",
			"http://www.google.com", "", false,
			bytes.NewReader([]byte(`{"targetUrl": "http://www.google.com", "name": ""}`)),
			http.StatusOK, "",
		},
		{
			"It should return error in case read error",
			"http://bluefield.com", "bluefield shortcut", false,
			newErrorableReader(
				[]byte(`{"targetUrl": "http://bluefield.com", "name": "bluefield shortcut"}`),
				errors.New("READ_BODY_ERROR")),
			http.StatusInternalServerError, "READ_BODY_ERROR",
		},
		{
			"It should return error in case invalid input json",
			"http://bluefield.com", "bluefield shortcut", false,
			bytes.NewReader([]byte(`["targetUrl": "http://bluefield.com", "name": "bluefield shortcut"]`)),
			http.StatusBadRequest, "INVALID_BODY",
		},
		{
			"It should return error in case invalid url",
			"http://bluefield.com", "bluefield shortcut", false,
			bytes.NewReader([]byte(`{"targetUrl": "http//bluefield!.com", "name": "bluefield shortcut"}`)),
			http.StatusBadRequest, "INVALID_URL",
		},
	}

	for _, it := range cases {
		t.Run(fmt.Sprintf("%s", it.name), func(t *testing.T) {
			//Arrange
			mockManager := &mocks.IUrlShortenerManager{}
			mockLinkRepo := &mocks.ICachedLinksRepository{}

			targetUrlObject, _ := url.Parse(it.targetUrl)
			expectedLinkModel := model.NewLink(targetUrlObject, it.linkName, nil, nil)

			expectedShortUrl, _ := manager.UrlShortener.GenerateShortUrl(expectedLinkModel)

			mockLinkRepo.On(
				"CreateLink",
				mock.Anything, mock.AnythingOfType("string"),
			).Return(expectedLinkModel, it.isNew, nil)
			mockManager.On("GenerateShortUrl", mock.Anything).
				Return(expectedShortUrl, nil)

			controller := urlsController{mockManager, mockLinkRepo}
			responseRecorder := httptest.NewRecorder()
			reqUrl, _ := url.Parse("http://localhost:8080/api/urls")
			mockReq := httptest.NewRequest(http.MethodPost, reqUrl.String(), it.inputBody)

			//Assert
			controller.handleCreate(responseRecorder, mockReq, httprouter.Params{})

			//Verify
			assert.Equal(t, it.expectedResponseCode, responseRecorder.Code,
				"Expected responseCode %v, but got %v", it.expectedResponseCode, responseRecorder.Code)

			responseBytes, _ := ioutil.ReadAll(responseRecorder.Body)
			response := string(responseBytes)

			expectedErrorBody := fmt.Sprintf(`{"error": "", "code": "%s"}`, it.expectedErrorCode)

			if (it.expectedResponseCode == http.StatusOK) || (it.expectedResponseCode == http.StatusCreated) {
				assert.Equal(t, responseRecorder.Header().Get("Content-Type"), "application/json",
					"Expected Content-Type: application/json header")

				expectedBodyBytes, _ := json.Marshal(viewmodel.LinkResponseViewModel{
					Hash: expectedLinkModel.Hash, Name: it.linkName, TargetUrl: it.targetUrl, ShortUrl: expectedShortUrl.String(),
					CreatedAt: expectedLinkModel.CreatedAt.UnixNano(), UpdatedAt: expectedLinkModel.UpdatedAt.UnixNano(),
				})
				expectedBody := string(expectedBodyBytes)

				assert.Equal(t, expectedBody, response,
					"Expected response json to equal %q, but got %q", expectedBody, response)
			} else {
				assert.Equal(t, expectedErrorBody, response,
					"Expected error body %q, but got %q", expectedErrorBody, response)
			}
		})
	}
}

func TestUrlsController_handleRedirect(t *testing.T) {
	cases := []struct {
		name      string
		targetUrl string

		expectedResponseCode int
		expectedErrorCode    string
	}{
		{
			"It should redirect to target url by provided url hash",
			"http://www.google.com",

			http.StatusFound, "",
		},
		{
			"It should respond with 404 status code for non existing url",
			"https://non-existing.google.com",
			http.StatusNotFound, "NOT_FOUND",
		},
		{
			"It should respond with 500 status code when db is unavailable",
			"https://golang.com",
			http.StatusInternalServerError, "INTERNAL_SERVER_ERROR",
		},
	}

	for _, it := range cases {
		t.Run(fmt.Sprintf("%s", it.name), func(t *testing.T) {
			//Arrange
			mockManager := &mocks.IUrlShortenerManager{}
			mockLinkRepo := &mocks.ICachedLinksRepository{}

			targetUrlObject, _ := url.Parse(it.targetUrl)
			urlHash, _ := manager.UrlShortener.GenerateShortUrl(model.NewLink(targetUrlObject, "", nil, nil))

			if it.expectedErrorCode != "" {
				mockLinkRepo.On("GetTargetUrlByHash", mock.AnythingOfType("string")).
					Return("", errors.New(it.expectedErrorCode))
			} else {
				mockLinkRepo.On("GetTargetUrlByHash", mock.AnythingOfType("string")).
					Return(it.targetUrl, nil)
			}

			controller := urlsController{mockManager, mockLinkRepo}
			responseRecorder := httptest.NewRecorder()
			reqUrl, _ := url.Parse(fmt.Sprintf("http://localhost:8080/api/%s", urlHash.String()))
			mockReq := httptest.NewRequest(http.MethodGet, reqUrl.String(), nil)

			//Assert
			controller.handleRedirect(responseRecorder, mockReq, httprouter.Params{{
				Key:   "urlHash",
				Value: urlHash.String(),
			}})

			//Verify
			assert.Equal(t, it.expectedResponseCode, responseRecorder.Code,
				"Expected responseCode %v, but got %v", it.expectedResponseCode, responseRecorder.Code)

			responseBytes, _ := ioutil.ReadAll(responseRecorder.Body)
			response := string(responseBytes)

			expectedErrorBody := fmt.Sprintf(`{"error": "", "code": "%s"}`, it.expectedErrorCode)
			if it.expectedResponseCode == http.StatusFound {
				assert.Equal(t, responseRecorder.Header().Get("Location"), it.targetUrl,
					"Expected Location: %q header", it.targetUrl)
			} else {
				assert.Equal(t, expectedErrorBody, response,
					"Expected error body %q, but got %q", expectedErrorBody, response)
			}
		})
	}
}

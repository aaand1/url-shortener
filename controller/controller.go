package controller

import (
	"bluefield/url-shortener/config"
	"bluefield/url-shortener/db"
	"bluefield/url-shortener/manager"
	"bluefield/url-shortener/repository"
	"github.com/julienschmidt/httprouter"
	"html/template"
)

var (
	urls = &urlsController{&manager.UrlShortenerManager{},
		repository.NewCachedLinksRepository(repository.NewLinksRepository(nil), config.Get().CacheExpiration, config.Get().CacheCleanupInterval)}
	home = &homeController{}
)

func InitControllers() {
	urls.UrlShortenerManager.SetDbContext(db.Get())
	urls.LinksRepo.SetDbContext(db.Get())
}

func Startup(templates map[string]*template.Template, appRouter *httprouter.Router, apiRouter *httprouter.Router) {
	InitControllers()

	urls.registerRoutes(appRouter, apiRouter)
	home.registerRoutes(templates, appRouter)

	appRouter.NotFound = apiRouter
}

package controller

import (
	"bluefield/url-shortener/log"
	"bluefield/url-shortener/manager"
	"bluefield/url-shortener/repository"
	"bluefield/url-shortener/viewmodel"
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type urlsController struct {
	UrlShortenerManager manager.IUrlShortenerManager
	LinksRepo           repository.ILinksRepository
}

func (c *urlsController) registerRoutes(appRouter *httprouter.Router, apiRouter *httprouter.Router) {
	//appRouter.GET("/api/urls", c.handleGetAll)
	appRouter.POST("/api/urls", c.handleCreate)
	appRouter.DELETE("/api/urls/:urlHash", c.handleDelete)
	appRouter.GET("/api/:urlHash", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		if p.ByName("urlHash") == "urls" {
			c.handleGetAll(w, r, p)
		} else {
			c.handleRedirect(w, r, p)
		}
	})
}

func (c *urlsController) handleGetAll(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	var limit, offset uint = 10, 0
	var query = ""
	queryValues := r.URL.Query()
	if val, err := strconv.Atoi(queryValues.Get("limit")); err == nil &&
		val > 0 {
		limit = uint(val)
	}
	if val, err := strconv.Atoi(queryValues.Get("offset")); err == nil &&
		val > 0 {
		offset = uint(val)
	}
	var createdBy = "unknown"
	linkModels, err := c.LinksRepo.GetLinks(query, createdBy, limit, offset)
	if err != nil {
		ResponseError(w, "INTERNAL_SERVER_ERROR", http.StatusInternalServerError, err)
		return
	}

	items := viewmodel.LinksToVMs(linkModels, c.UrlShortenerManager)
	response, err := json.Marshal(items)
	if err != nil {
		ResponseError(w, "INTERNAL_SERVER_ERROR", http.StatusInternalServerError, err)
		return
	}
	log.Logger.Debug("Writing response: ", string(response))
	w.Header().Add("Content-Type", "application/json")
	w.Write(response)
}

func (c *urlsController) handleCreate(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ResponseError(w, "READ_BODY_ERROR", http.StatusInternalServerError, err)
		return
	}

	vm, err := viewmodel.CreateUrlViewModel(data)
	if err != nil {
		ResponseError(w, "INVALID_BODY", http.StatusBadRequest, err)
		return
	}
	targetUrl, err := url.ParseRequestURI(vm.TargetUrl)
	if err != nil {
		ResponseError(w, "INVALID_URL", http.StatusBadRequest, err)
		return
	}

	linkModel, isNew, err := c.LinksRepo.CreateLink(targetUrl, vm.Name)
	if err != nil {
		ResponseError(w, "INTERNAL_SERVER_ERROR", http.StatusInternalServerError, err)
		return
	}

	viewModel := viewmodel.LinkToVM(linkModel, c.UrlShortenerManager)
	response, err := json.Marshal(viewModel)
	if err != nil {
		ResponseError(w, "INTERNAL_SERVER_ERROR", http.StatusInternalServerError, err)
		return
	}
	if isNew {
		w.WriteHeader(http.StatusCreated)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	log.Logger.Debug("Writing create response: ", string(response))

	w.Header().Add("Content-Type", "application/json")
	w.Write(response)
}

func (c *urlsController) handleDelete(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	var linkHash = p.ByName("urlHash")

	if strings.TrimSpace(linkHash) == "" {
		ResponseError(w, "INVALID_LINK_HASH", http.StatusBadRequest, nil)
		return
	}

	if err := c.LinksRepo.DeleteLink(linkHash); err != nil {
		ResponseError(w, "INTERNAL_SERVER_ERROR", http.StatusInternalServerError, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (c *urlsController) handleRedirect(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	log.Logger.Printf("Handling redirect to %s", p.ByName("urlHash"))
	hash := p.ByName("urlHash")
	targetUrl, err := c.LinksRepo.GetTargetUrlByHash(hash)
	if err != nil {
		if err.Error() == "NOT_FOUND" {
			ResponseError(w, "NOT_FOUND", http.StatusNotFound, err)
			return
		}
		ResponseError(w, "INTERNAL_SERVER_ERROR", http.StatusInternalServerError, err)
		return
	}

	http.Redirect(w, r, targetUrl, http.StatusFound)
}

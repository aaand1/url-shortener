// +build integration

package controller

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"testing"
)

type integrationTestsConfig struct {
	UrlShortenerServiceBaseUrl string `envconfig:"SERVICE_BASE_URL" default:"http://localhost:8080/"`
}

var (
	runConfig = &integrationTestsConfig{}
)

func TestMain(m *testing.M) {
	envconfig.MustProcess("URL_SHORTENER_INTEGRATION_TESTS", runConfig)
	result := m.Run()
	os.Exit(result)
}

func mustMarshall(obj interface{}) []byte {
	res, err := json.Marshal(obj)
	if err != nil {
		panic(err)
	}
	return res
}

type UrlsGetAllSuite struct {
	suite.Suite

	urls  []string
	links []map[string]interface{} //ordered by createdAt links slice
	//linksIndex map[string]map[string]interface{} //map[linkUrl] -> link
}

func (s *UrlsGetAllSuite) SetupSuite() {
	s.urls = []string{
		"https://en.wikipedia.org/wiki/A_Tribe_Called_Quest",
		"https://en.wikipedia.org/wiki/Run-DMC",
		"https://en.wikipedia.org/wiki/Beastie_Boys",
		"https://en.wikipedia.org/wiki/Everlast_(musician)",
		"https://en.wikipedia.org/wiki/Phife_Dawg",
		"https://en.wikipedia.org/wiki/Jurassic_5",
		"https://en.wikipedia.org/wiki/The_Roots",
		"https://en.wikipedia.org/wiki/Rage_Against_the_Machine",
		"https://en.wikipedia.org/wiki/Prophets_of_Rage",
		"https://en.wikipedia.org/wiki/Plan_B",
		"http://www.gopl.io/ch1.pdf",
	}

	s.links = ([]map[string]interface{}{})

	s.postLinks()
}

func (s *UrlsGetAllSuite) TearDownSuite() {
	s.deleteLinks()
}

func (s *UrlsGetAllSuite) BeforeTest(suiteName, testName string) {

}

func (s *UrlsGetAllSuite) AfterTest(suiteName, testName string) {

}

func (s *UrlsGetAllSuite) postLinks() {
	wg := &sync.WaitGroup{}

	for i, url := range s.urls {
		wg.Add(1)
		go func(url string, i int) {
			resp, err := http.Post(runConfig.UrlShortenerServiceBaseUrl+"api/urls", "application/json", bytes.NewReader(mustMarshall(map[string]string{
				"targetUrl": url, "name": fmt.Sprintf("url%d", i),
			})))
			if err != nil {
				fmt.Printf("FAILED TO CREATE %d LINK(%s). err=%s", i, url, err)
				return
			}

			defer resp.Body.Close()

			var link = map[string]interface{}{}
			if err = json.NewDecoder(resp.Body).Decode(&link); err != nil {
				fmt.Printf("json.Decode error: i=%d url=%s, err=%s", i, url, err)
				return
			}

			s.links = append(s.links, link)
			wg.Done()
		}(url, i)
	}

	wg.Wait()

	sort.Slice(s.links, func(i, j int) bool {
		var el1, el2 = s.links[i], s.links[j]
		cr1 := el1["createdAt"].(float64)
		cr2 := el2["createdAt"].(float64)

		if cr1 == cr2 {
			return strings.Compare(el1["name"].(string), el2["name"].(string)) == -1
		}
		return cr1 < cr2
	})
}

func (s *UrlsGetAllSuite) deleteLinks() {
	wg := &sync.WaitGroup{}

	for i, linkItem := range s.links {
		wg.Add(1)

		go func(linkItem map[string]interface{}, i int) {
			request, _ := http.NewRequest(http.MethodDelete,
				runConfig.UrlShortenerServiceBaseUrl+"api/urls/"+(linkItem["hash"].(string)), nil)
			resp, err := http.DefaultClient.Do(request)
			defer resp.Body.Close()

			if err != nil {
				fmt.Printf("FAILED TO DELETE %d LINK(%s). err=%s", i, linkItem, err)
				require.NoError(s.T(), err)
			}
			wg.Done()
		}(linkItem, i)
	}

	wg.Wait()
}

func (s *UrlsGetAllSuite) Test_urls_GetAll() {
	type args struct {
		offset, limit *int

		expectedStatusCode int
		expectedResponse   []map[string]interface{}
	}

	getLinks := func(offset, limit int) []map[string]interface{} {
		return s.links[offset : offset+limit]
	}

	tests := []struct {
		name string
		args args
	}{
		{
			"It should get first 10 links in system by default",
			args{
				nil, nil,
				http.StatusOK,
				getLinks(0, 10),
			},
		}, {
			"It should take [2;8] items/?offset=2&limit=8 links",
			args{
				&[]int{2}[0], &[]int{8}[0],
				http.StatusOK,
				getLinks(2, 8),
			},
		},
	}

	for _, it := range tests {
		s.T().Run(it.name, func(t *testing.T) {
			//Arrange
			queryString := url.Values{}
			if it.args.limit != nil {
				queryString.Set("limit", strconv.Itoa(*it.args.limit))
			}
			if it.args.offset != nil {
				queryString.Set("offset", strconv.Itoa(*it.args.offset))
			}
			reqUrl, _ := url.Parse(runConfig.UrlShortenerServiceBaseUrl + "api/urls?" + (queryString).Encode())

			//Assert
			resp, err := http.Get(reqUrl.String())
			defer resp.Body.Close()

			//Verify
			require.NoError(t, err)

			assert.Equal(t, resp.StatusCode, it.args.expectedStatusCode,
				"expected status code to equal %s, but got %s", it.args.expectedStatusCode, resp.StatusCode)

			response := []map[string]interface{}{}
			err = json.NewDecoder(resp.Body).Decode(&response)
			require.NoError(t, err)

			assert.Len(t, response, len(it.args.expectedResponse),
				"expected len(response) to be %s, but got %s", len(it.args.expectedResponse), len(response))
			assert.Equal(t, it.args.expectedResponse, response,
				"expected response body to equal %s, but got %s", it.args.expectedResponse, response)
		})
	}
}

func Test_urls_GetAll(t *testing.T) {
	suite.Run(t, new(UrlsGetAllSuite))
}

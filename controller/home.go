package controller

import (
	"github.com/julienschmidt/httprouter"
	"html/template"
	"net/http"
)

type homeController struct {
	homeTemplate *template.Template
}

func (h *homeController) registerRoutes(templates map[string]*template.Template, appRouter *httprouter.Router) {
	h.homeTemplate = templates["index.html"]
	appRouter.GET("/", h.handleHome)

	appRouter.ServeFiles("/img/*filepath", http.Dir("public"))
	appRouter.ServeFiles("/css/*filepath", http.Dir("public"))
}

func (h *homeController) handleHome(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if err := h.homeTemplate.Execute(w, struct {
		Title string
	}{
		Title: "Create short link",
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

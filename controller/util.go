package controller

import (
	"bluefield/url-shortener/config"
	"bluefield/url-shortener/log"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"strings"
)

func ResponseError(w http.ResponseWriter, errorCode string, statusCode int, err error) {
	if err != nil {
		log.Logger.Errorf("Response error: %v", err)
	}
	errStr := ""
	if config.Get().LogLevel == logrus.DebugLevel {
		errStr = err.Error()
	}

	responseBody := fmt.Sprintf(`{"error": "%s", "code": "%s"}`, errStr, errorCode)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	fmt.Fprintf(w, responseBody)
}

func getQueryParam(vals *url.Values, key string) string {
	key = strings.TrimSpace(key)
	if len(key) == 0 {
		return ""
	}

	if val, ok := (*vals)[key]; ok && len(val) > 0 {
		return val[0]
	}
	return ""
}

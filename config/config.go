package config

import (
	"errors"
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
	"net/url"
	"sync/atomic"
	"time"
)

type BaseUrlDecoder url.URL

func (urlDecoder *BaseUrlDecoder) Decode(rawUrl string) error {
	value, err := url.ParseRequestURI(rawUrl)
	if err != nil {
		return err
	}

	*urlDecoder = BaseUrlDecoder(*value)

	return nil
}

func (urlDecoder *BaseUrlDecoder) String() string {
	var rawUrl = url.URL(*urlDecoder)
	return rawUrl.String()
}

var (
	configAtomic = atomic.Value{}
)

type DbSourceType string

const (
	DbTypeSqlite DbSourceType = "sqlite3"
	DbTypePgSql  DbSourceType = "postgres"
)

func (sourceType *DbSourceType) Decode(rawType string) error {
	if rawType == "sqlite3" {
		*sourceType = DbTypeSqlite
		return nil
	} else if rawType == "pgsql" {
		*sourceType = DbTypePgSql
		return nil
	}

	return errors.New("Unknown type: " + rawType)
}

type AppConfig struct {
	ServiceHost          string         `default:"localhost"`
	ServicePort          uint           `default:"80"`
	CacheExpiration      time.Duration  `default:"10m"`
	CacheCleanupInterval time.Duration  `default:"5m"`
	LogLevel             logrus.Level   `default:"info"`
	RedirectUrlBasePath  BaseUrlDecoder `required:"true"`   //eg: "http://localhost:80/api/"
	DbSourceType         DbSourceType   `default:"sqlite3"` //eg: "postgres", "sqlite3"
	ConnectionString     string         `required:"true"`   //eg: "./url-shortener.db"
}

func Init() (*AppConfig, error) {
	if cfg := configAtomic.Load(); cfg != nil {
		val := cfg.(AppConfig)
		return &val, nil
	}
	var config AppConfig
	err := envconfig.Process("URL_SHORTENER", &config)
	if err != nil {
		return nil, err
	}

	Set(config)
	return &config, nil
}

func Get() AppConfig {
	config, _ := configAtomic.Load().(AppConfig)
	return config
}

func Set(cfg AppConfig) {
	configAtomic.Store(cfg)
}

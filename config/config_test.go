//+build unit

package config

import (
	"errors"
	"fmt"
	"net/url"
	"testing"
)

func TestBaseUrlDecoder_Decode(t *testing.T) {
	cases := []struct {
		name   string
		rawUrl string
		err    error
	}{
		{
			"Should parse plain url",
			"https://localhost:8080/",
			nil,
		},
		{
			"Should parse url with path",
			"https://localhost:8080/api",
			nil,
		},
		{
			"Should fail to parse invalid baseUrl",
			"http//localhost",
			error(&url.Error{URL: "http//localhost", Op: "parse", Err: errors.New("Error")}),
		},
	}

	for _, it := range cases {
		t.Run(fmt.Sprintf("%s : %s", it.name, it.rawUrl), func(t *testing.T) {
			var baseUrlDecoder BaseUrlDecoder
			err := baseUrlDecoder.Decode(it.rawUrl)

			if it.err == nil {
				if err != nil || baseUrlDecoder.String() != it.rawUrl {
					t.Fatalf("Expected decoder to decode to %v, but got %v. error = %v, expected error = %v",
						it.rawUrl, baseUrlDecoder.String(), err, it.err)
				}
			} else {
				if err == nil || baseUrlDecoder.String() != "" {
					t.Fatalf("Expected decoder to fail with error %v, but decoded url %v to %v. error = %v", it.err, it.rawUrl, baseUrlDecoder.String(), err)
				}
			}
		})
	}
}

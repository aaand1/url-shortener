package main

import (
	"bluefield/url-shortener/config"
	"bluefield/url-shortener/db"
	"bluefield/url-shortener/log"
	"context"
	"html/template"
	"io/ioutil"
	stdlog "log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"bluefield/url-shortener/controller"
	"github.com/julienschmidt/httprouter"
)

/**
TOOD:
 1. Integrate mongo
  1.1 Refactor pgsql <-> sqlite switching with "orm db access factory"
 2. Rewrite to github.com/gin-gonic/gin
 3. Add authorization and role based api access
 4. Add modules support
 2. Increase integration tests coverage for REST APIs
 	2.1 Update integration tests structure
 3. Add unit tests for db layers
 4. Add react-ui
 5. Add logging
 6. Test db connection handling
 7. Add container integration
  7.1. Add docs generation
*/

func main() {
	appRouter, apiRouter := httprouter.New(), httprouter.New()

	appConfig, err := config.Init()
	stdlog.Print("Initialized config: ", appConfig)
	if err != nil {
		stdlog.Fatal(err)
	}
	output := os.Stdout
	log.Init(appConfig.LogLevel, output, "" /*"/tmp/url_shortener.log"*/)
	log.Logger.Debugf("Initialized logger: level=%s, output=%s; logger=%v\n", appConfig.LogLevel, output, log.Logger)

	ctx := db.Get()
	if ctx == nil {
		log.Logger.Fatalf("Db context creation failed. ConnectionString=%v", appConfig.ConnectionString)
	} else if err := ctx.InitContext(); err != nil {
		log.Logger.Fatalf("Db context initialization failed. ConnectionString=%v, error=%v", appConfig.ConnectionString, err)
	}
	db.Set(ctx)
	log.Logger.Debugf("Initialized db context: %v", db.Get())

	controller.Startup(populateTemplates(), appRouter, apiRouter)
	server := http.Server{Addr: appConfig.ServiceHost + ":" + strconv.FormatUint(uint64(appConfig.ServicePort), 10), Handler: appRouter}

	log.Logger.Printf("Starting http server at: %s:%d ...\n", appConfig.ServiceHost, appConfig.ServicePort)

	shutdownChannel := make(chan os.Signal, 1)
	go listenShutdownSignal(shutdownChannel, func(shutdownSignal os.Signal) {
		log.Logger.Println("Shutting down the server...")

		ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*5)
		server.Shutdown(ctx)
		defer cancelFunc()
	})
	log.Logger.Fatal(server.ListenAndServe())
}

func listenShutdownSignal(inputChannel chan os.Signal, onShutdown func(os.Signal)) {
	signal.Notify(inputChannel, os.Kill, os.Interrupt)

	inputSignal := <-inputChannel
	if inputSignal == os.Kill || inputSignal == os.Interrupt {
		onShutdown(inputSignal)
	}
}

func populateTemplates() map[string]*template.Template {
	result := make(map[string]*template.Template)
	const basePath = "templates"
	layout := template.Must(template.ParseFiles(basePath + "/_layout.html"))
	template.Must(layout.ParseFiles(basePath+"/_header.html", basePath+"/_footer.html"))
	dir, err := os.Open(basePath + "/content")
	if err != nil {
		panic("Failed to open template blocks directory: " + err.Error())
	}
	fis, err := dir.Readdir(-1)
	if err != nil {
		panic("Failed to read contents of content directory: " + err.Error())
	}

	for _, fi := range fis {
		f, err := os.Open(basePath + "/content/" + fi.Name())
		if err != nil {
			panic("Failed to open template '" + fi.Name() + "'")
		}
		content, err := ioutil.ReadAll(f)
		if err != nil {
			panic("Failed to read content from file '" + fi.Name() + "'")
		}
		f.Close()
		tmpl := template.Must(layout.Clone())
		_, err = tmpl.Parse(string(content))
		if err != nil {
			panic("Failed to parse contents of '" + fi.Name() + "' as template")
		}
		result[fi.Name()] = tmpl
	}
	return result
}

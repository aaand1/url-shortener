package log

import (
	"github.com/sirupsen/logrus"
	"io"
	"log"
	"os"
	"path"
	"strings"
)

var Logger IAppLogger = NewLogger(logrus.StandardLogger().Level, logrus.StandardLogger().Formatter, os.Stdout)

func Init(level logrus.Level, output io.Writer, outputFilePath string) {
	if strings.TrimSpace(outputFilePath) != "" {
		if fileOutput, err := os.OpenFile(path.Join(outputFilePath), os.O_WRONLY|os.O_CREATE, 0755); err != nil {
			log.Fatalln("logger.Init error: ", err, outputFilePath)
		} else {
			output = fileOutput
			//defer fileOutput.Close()
		}
	}
	Logger.SetLevel(level)
	//Logger.SetFormatter(formatter)
	Logger.SetOutput(output)
}

type IAppLogger interface {
	//logrus.StdLogger //FieldLogger conflict, there no std log compatibility
	SetLevel(logrus.Level)
	SetOutput(io.Writer)
	SetFormatter(logrus.Formatter)
	logrus.FieldLogger

	DbLogger() IAppLogger
}

func NewLogger(level logrus.Level, formatter logrus.Formatter, output io.Writer) IAppLogger {
	std := logrus.New()
	std.SetLevel(level)
	std.SetFormatter(formatter)
	std.SetOutput(output)

	return &AppLogger{*std}
}

type AppLogger struct {
	logrus.Logger
}

func (l *AppLogger) Print(values ...interface{}) {
	l.Logger.Debug(values...)
}

func (l *AppLogger) DbLogger() IAppLogger {
	return l
}

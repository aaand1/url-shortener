package db

import (
	"bluefield/url-shortener/config"
	"bluefield/url-shortener/log"
	"bluefield/url-shortener/model"
	"database/sql"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	_ "github.com/mattn/go-sqlite3"
	"github.com/sirupsen/logrus"
	stdlog "log"
	"strings"
	"time"
)

type IDbContext interface {
	InitContext() error
	FindLink(string) (*Link, error)
	FindLinks(string, string, uint, uint) (*[]Link, error)
	UpsertLink(model *model.Link) (bool, error)
	GetTargetUrlByHash(linkHash string) (string, error)
	DeleteLink(linkHash string) error
}

type IDbContextOwner interface {
	DbContext() IDbContext
	SetDbContext(ctx IDbContext)
}

type (
	Link struct {
		Name      string    `gorm:"type:VARCHAR(64);"`
		TargetUrl string    `gorm:"column:targetUrl;type:TEXT;PRIMARY_KEY"`
		Hash      string    `gorm:"type:VARCHAR(64);NOT NULL"`
		CreatedBy string    `gorm:"column:createdBy;type:VARCHAR(64);NOT NULL"`
		CreatedAt time.Time `gorm:"column:createdAt;type:datetime;NOT NULL"`
		UpdatedAt time.Time `gorm:"column:updatedAt;type:datetime;NOT NULL"`
	}
)

type IDb interface {
	Prepare(string) (*sql.Stmt, error)
}

type DbContext struct {
	DB *gorm.DB
}

type upsertOption func(*gorm.DB)

var context IDbContext

func NewContext(storageType config.DbSourceType, pgSqlCommon gorm.SQLCommon) (IDbContext, error) {
	var db *gorm.DB
	var err error
	var connString interface{}
	connString = config.Get().ConnectionString
	if pgSqlCommon != nil {
		connString = pgSqlCommon
	}

	if storageType == config.DbTypeSqlite {
		db, err = gorm.Open("sqlite3", connString)
	} else if storageType == config.DbTypePgSql {
		db, err = gorm.Open("postgres", connString)
	}
	if err != nil {
		return nil, err
	}

	if cfg := config.Get(); cfg.LogLevel != logrus.WarnLevel {
		db.LogMode(true)
	} else {
		db.LogMode(false)
	}

	db.SetLogger(log.Logger.DbLogger())

	if storageType == config.DbTypePgSql {
		return &PgSqlDbContext{DbContext{db}}, nil
	}

	return &DbContext{db}, nil
}

func Get() IDbContext {
	if context != nil {
		return context
	}
	context, err := NewContext(config.Get().DbSourceType, nil)
	if err != nil {
		stdlog.Printf("Failed to initialize db context. Error=%v", err)
		return nil
	}
	return context
}

func Set(ctx IDbContext) {
	context = ctx
}

func (ctx *DbContext) doUpsertLink(model *model.Link, beforeUpsert *upsertOption) (*Link, error) {
	if beforeUpsert == nil {
		var defaultBeforeUpsert = (upsertOption)(func(newDb *gorm.DB) {
			insertOption := `ON CONFLICT ("targetUrl") DO UPDATE SET "updatedAt"=excluded."updatedAt"`
			*newDb = *newDb.Set("gorm:insert_option", insertOption)
		})
		beforeUpsert = &defaultBeforeUpsert
	}
	createdLink := &Link{
		Name: model.Name, TargetUrl: model.TargetUrl.String(), Hash: model.Hash,
		CreatedBy: "unknown", CreatedAt: model.CreatedAt, UpdatedAt: model.UpdatedAt,
	}

	newDb := ctx.DB
	(*(beforeUpsert))(newDb)

	newDb = newDb.Create(createdLink)
	if newDb.Error != nil {
		return nil, newDb.Error
	}

	existingLink, err := ctx.FindLink(model.Hash)
	if err != nil {
		return nil, err
	}
	if existingLink == nil {
		return nil, fmt.Errorf("LINK_NOT_FOUND_ERROR")
	}

	return existingLink, nil
}

func (ctx *DbContext) InitContext() error {
	ctx.DB = ctx.DB.AutoMigrate(&Link{})

	if ctx.DB.Error != nil {
		return ctx.DB.Error
	}

	return nil
}

func (ctx *DbContext) FindLink(hash string) (result *Link, err error) {
	result = &Link{}

	if err := ctx.DB.
		//Where("hash = ?", hash).
		//Select([]string{"\"targetUrl\""}).
		First(result, `hash = ?`, hash).Error; err != nil {
		return nil, err
	}

	return
}

func (ctx *DbContext) FindLinks(nameQuery, createdBy string, limit, offset uint) (*[]Link, error) {
	items := []Link{}

	queryDb := ctx.DB.Limit(limit).Offset(offset).
		Select(`name, "targetUrl", hash, "createdBy", "createdAt", "updatedAt"`).
		Order(`"createdAt" asc, name asc`)

	if len(strings.TrimSpace(nameQuery)) == 0 {
		queryDb = queryDb.Where(`"createdBy" = ?`, createdBy)
	} else {
		queryDb = queryDb.Where(`"createdBy" = ? and name LIKE ?`, createdBy, nameQuery)
	}

	if err := queryDb.Find(&items).Error; err != nil {
		return nil, err
	}

	return &items, nil
}

func (ctx *DbContext) UpsertLink(model *model.Link) (bool, error) {
	var PgAndSqliteBeforeUpsertHook = (upsertOption)(func(newDb *gorm.DB) {
		insertOption := `ON CONFLICT ("targetUrl") DO UPDATE SET "updatedAt"=excluded."updatedAt"`
		*newDb = *newDb.Set("gorm:insert_option", insertOption)
	})
	newLink, err := ctx.doUpsertLink(model, &PgAndSqliteBeforeUpsertHook)

	if err != nil {
		return false, err
	}

	isNew := true
	if newLink.UpdatedAt == model.UpdatedAt && newLink.CreatedAt != model.CreatedAt {
		isNew = false
	}

	log.Logger.Debugf("COPYING: newLink[name=%s].CreatedAt=%s ->> model[name=%s].CreatedAt=%s",
		newLink.Name, newLink.CreatedAt,
		model.Name, model.CreatedAt)
	model.Name = newLink.Name
	model.CreatedAt = newLink.CreatedAt
	model.UpdatedAt = newLink.UpdatedAt

	return isNew, nil
}

func (ctx *DbContext) GetTargetUrlByHash(linkHash string) (string, error) {
	link := &Link{}

	if err := ctx.DB.
		Where("hash = ?", linkHash).
		Select([]string{"\"targetUrl\""}).
		First(link).Error; err != nil {
		return "", err
	}

	return link.TargetUrl, nil
}

func (ctx *DbContext) DeleteLink(linkHash string) error {
	if err := ctx.DB.Delete(&Link{}, `hash = ?`, linkHash).Error; err != nil {
		return err
	}

	return nil
}

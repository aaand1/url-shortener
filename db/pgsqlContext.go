package db

import (
	"bluefield/url-shortener/log"
	"bluefield/url-shortener/model"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	_ "github.com/mattn/go-sqlite3"
	"time"
)

type PgSqlDbContext struct {
	DbContext
}

type (
	//CreatedAt/UpdatedAt requires separate pgsql type and therefore new model
	pgLink struct {
		Name      string    `gorm:"type:VARCHAR(64);"`
		TargetUrl string    `gorm:"column:targetUrl;type:TEXT;PRIMARY_KEY"`
		Hash      string    `gorm:"type:VARCHAR(64);NOT NULL"`
		CreatedBy string    `gorm:"column:createdBy;type:VARCHAR(64);NOT NULL"`
		CreatedAt time.Time `gorm:"column:createdAt;type:timestamp(6) without time zone;NOT NULL"`
		UpdatedAt time.Time `gorm:"column:updatedAt;type:timestamp(6) without time zone;NOT NULL"`
	}
)

func (*pgLink) TableName() string {
	return "links"
}

func (ctx *PgSqlDbContext) InitContext() error {
	ctx.DB = ctx.DB.AutoMigrate(&pgLink{})

	if ctx.DB.Error != nil {
		return ctx.DB.Error
	}

	return nil
}

func (ctx *PgSqlDbContext) UpsertLink(model *model.Link) (bool, error) {
	newLink, err := ctx.DbContext.doUpsertLink(model, (*upsertOption)(nil))
	if err != nil {
		return false, err
	}

	isNew := true
	if isEqualPgTime(&newLink.UpdatedAt, &model.UpdatedAt) && !isEqualPgTime(&newLink.CreatedAt, &model.CreatedAt) {
		isNew = false
	}

	log.Logger.Debugf("COPYING: newLink[name=%s].CreatedAt=%s ->> model[name=%s].CreatedAt=%s",
		newLink.Name, newLink.CreatedAt,
		model.Name, model.CreatedAt)
	model.Name = newLink.Name
	model.CreatedAt = newLink.CreatedAt
	model.UpdatedAt = newLink.UpdatedAt

	return isNew, nil
}

func isEqualPgTime(pgTime, linkTime *time.Time) bool {
	//Truncate nanoseconds as PG server store nanoseconds in different precision than in golang#gorm(6 vs 9)
	const truncatePrecision = time.Microsecond
	return pgTime.Truncate(truncatePrecision).UnixNano() == linkTime.Truncate(truncatePrecision).UnixNano()
}

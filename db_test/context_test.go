//+build unit

package db_test

import (
	"bluefield/url-shortener/config"
	"bluefield/url-shortener/db"
	"bluefield/url-shortener/model"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	_ "github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
	"net/url"
	"regexp"
	"testing"
)

func Test_IDbContext_GetTargetUrlByHash(t *testing.T) {
	cases := []struct {
		name      string
		targetUrl string
	}{
		{
			"It should sanitize linkHash param",
			"https://www.google.com",
		},
	}

	for _, it := range cases {
		for _, d := range [...]config.DbSourceType{config.DbTypePgSql, config.DbTypeSqlite} {
			t.Run(fmt.Sprintf("%s(%s)", it.name, d), func(t *testing.T) {
				sqlMockdb, dbMock, err := sqlmock.New()
				if err != nil {
					assert.Error(t, err, "failed to open sqlmock database")
				}
				defer sqlMockdb.Close()

				dbContext, err := db.NewContext(d, sqlMockdb)
				if err != nil {
					assert.Error(t, err, "db.NewContext failed")
				}

				targetUrl, _ := url.ParseRequestURI(it.targetUrl)
				hash := model.NewLink(targetUrl, "", nil, nil).Hash
				excapeSymbol := "$1"
				if d == config.DbTypeSqlite {
					excapeSymbol = "?"
				}

				dbMock.ExpectQuery(
					regexp.QuoteMeta(fmt.Sprintf(
						`SELECT "targetUrl" FROM "links" WHERE (hash = %s) ORDER BY "links"."targetUrl" ASC LIMIT 1`, excapeSymbol))).
					WithArgs(hash).
					WillReturnRows(sqlmock.NewRows([]string{"targetUrl"}).
						AddRow(it.targetUrl))

				url, err := dbContext.GetTargetUrlByHash(hash)

				assert.NoError(t, err)
				assert.NoError(t, dbMock.ExpectationsWereMet())
				assert.Equal(t, it.targetUrl, url)
			})
		}
	}
}

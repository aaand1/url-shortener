# Url shorterer service

### How to run?

Server: 
```bash
:~$ chmod +x ./run.sh && ./run.sh
```

Unit tests:
```bash
:~$ go get github.com/vektra/mockery/ .../. &&  mockery -all && go test -v -tags=unit -race ./...
```

Integration tests:
```bash
:~$ export URL_SHORTENER_REDIRECTURLBASEPATH=http://localhost:8081
:~$ export URL_SHORTENER_CONNECTIONSTRING="host=localhost user=root password=toor dbname=url_shortener"
:~$ go run main.go &
:~$ echo $! >./pid.file
:~$ export URL_SHORTENER_INTEGRATION_TESTS_=http://localhost:8081/ 
:~$ go get github.com/vektra/mockery/ .../. &&  mockery -all && go test -v -tags=integration -race ./...
:~$ kill $(cat ./pid.file)
```

Generate mocks for tests:
```bash
:~$ go get github.com/vektra/mockery/.../. && mockery -name IDbContext -dir db && mockery -dir manager -name IUrlShortenerManager
``

### Configurations
See `./config/config.go` for details

| Env variable                          | Description                                                           | Required | Default    | Examples                                      |
|---------------------------------------|-----------------------------------------------------------------------|----------|----------- |-----------------------------------------------|
| URL_SHORTENER_SERVICEHOST             | Go server listen host                                                 |          | localhost  | 0.0.0.0                                       |
| URL_SHORTENER_SERVICEPORT             | Go server listen port                                                 |          | 8080       | 80                                            |
| URL_SHORTENER_CACHEEXPIRATION         | Time before deletion of link from cache                               |          | 10m        | 600s                                          |
| URL_SHORTENER_CACHECLEANUPINTERVAL    | Time for check whether to delete link from cache                      |          | 5m         | 300s                                          |
| URL_SHORTENER_LOGLEVEL                | Global service's log level                                            |          | info       | panic, fatal, error, warn, info, debug, trace |
| URL_SHORTENER_REDIRECTURLBASEPATH     | Base path for redirect url host                                       | +        |            | http://localhost:8080/api                     |
| URL_SHORTENER_DBSOURCETYPE            | Data source type                                                      |          |  sqlite3   | sqlite3, postgres                                 |
| URL_SHORTENER_CONNECTIONSTRING        | File system path to sqlite3 db file or postgresql connection string   | +        |            | "./url-shortener.db", "host=localhost user=root password=toor dbname=url_shortener" |

### API methods

| Method | Path                             | Description     | Body                                           | Sample response (CODE; CONTENT-TYPE|Location BODY)                                                                                                                                                                                                                          |
|--------|----------------------------------|-----------------|------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GET    | /                                | Home page       |                                                | 200 text/html                                                                                                                                                                                                                                                               |
| POST   | /api/urls                        | Create link     | {"targetUrl": "http://google.com", "name": ""} | 200/201 application/json { "targetUrl":"http://localhost:8080/?", "shortUrl":"http://localhost:8080/api/92dfc16b40fe2ac8419ae770b24846b5", "name":"", "createdAt":1553340063, "updatedAt":1553340063 }  400/500 text/html INVALID_BODY|INVALID_URL/INTERNAL_SERVER_ERROR    |
| DELETE | /api/urls/:urlHash               | Delete link     |                                                | 204  500 text/html INTERNAL_SERVER_ERROR                                                                                                                                                                                                                                    |
| GET    | /api/urls[?limit=10[&offset=0]]  | Get all urls    |                                                | 302 Location: http://localhost:8080/? 404 text/html INVALID_BODY  500 text/html INTERNAL_SERVER_ERROR                                                                                                                                                                       |
| GET    | /api/:urlHash                    | Create url hash |                                                | 302 Location: http://localhost:8080/? 404 text/html INVALID_BODY  500 text/html INTERNAL_SERVER_ERROR                                                                                                                                                                       |


## Requirements

- go@1.12
- sqlite3
- postgresql@11.3

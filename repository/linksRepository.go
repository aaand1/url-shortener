package repository

import (
	"bluefield/url-shortener/db"
	"bluefield/url-shortener/model"
	"net/url"
)

var LinkRepo ILinksRepository

type ILinksRepository interface {
	CreateLink(*url.URL, string) (*model.Link, bool, error)
	GetLinks(nameQuery, createdBy string, limit, offset uint) (*[]model.Link, error)

	GetTargetUrlByHash(string) (string, error)
	DeleteLink(string) error

	db.IDbContextOwner
}

type LinksRepository struct {
	dbContext db.IDbContext
}

func NewLinksRepository(dbContext db.IDbContext) ILinksRepository {
	return &LinksRepository{dbContext}
}

func (r *LinksRepository) DbContext() db.IDbContext {
	return r.dbContext
}

func (r *LinksRepository) SetDbContext(ctx db.IDbContext) {
	r.dbContext = ctx
}

func (r *LinksRepository) GetLinks(nameQuery string, createdBy string, limit, offset uint) (*[]model.Link, error) {
	links, err := r.dbContext.FindLinks(nameQuery, createdBy, limit, offset)
	if err != nil {
		return nil, err
	}
	models := make([]model.Link, len(*links))

	mustParse := func(raw string) (parsed *url.URL) {
		parsed, _ = url.ParseRequestURI(raw)
		if parsed == nil {
			panic("url parse error: " + raw)
		}
		return
	}
	for i, e := range *links {
		models[i] = *model.NewLink(mustParse(e.TargetUrl), e.Name, &e.CreatedAt, &e.UpdatedAt)
	}
	return &models, nil
}

func (r *LinksRepository) CreateLink(targetUrl *url.URL, name string) (linkModel *model.Link, isNew bool, err error) {
	linkModel = model.NewLink(targetUrl, name, nil, nil)

	isNew, err = r.dbContext.UpsertLink(linkModel)
	if err != nil {
		return nil, isNew, err
	}

	return linkModel, isNew, nil
}

func (r *LinksRepository) GetTargetUrlByHash(hash string) (string, error) {
	return r.dbContext.GetTargetUrlByHash(hash)
}

func (r *LinksRepository) DeleteLink(hash string) error {
	return r.dbContext.DeleteLink(hash)
}

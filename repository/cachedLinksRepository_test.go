//+build unit

package repository

import (
	"bluefield/url-shortener/mocks"
	"bluefield/url-shortener/model"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/url"
	"testing"
	"time"
)

func must(obj interface{}, err error) interface{} {
	if err != nil {
		panic(err)
	}
	return obj
}

func TestCachedLinksRepository_CreateLink(t *testing.T) {
	expiration, cleanupInterval := time.Duration(1*time.Second), time.Duration(2*time.Second)
	cases := []struct {
		name string

		targetUrl *url.URL
		urlName   string

		expectedError error
		expireAfter   time.Duration
	}{
		{
			"It should create, cache and remove on expiration the link on success creation",
			must(url.Parse("http://www.google.com")).(*url.URL),
			"google shortcut", nil, expiration,
		},
		{
			"It shouldn't cache url in case link creation error",
			must(url.Parse("http://www.failure.com")).(*url.URL),
			"", errors.New("CreateLink execution error"), -1,
		},
	}

	for _, it := range cases {
		t.Run(it.name, func(t *testing.T) {
			//Arrange
			mockSourceRepo := &mocks.ILinksRepository{}
			resultLink := model.NewLink(it.targetUrl, it.urlName, nil, nil)

			mockSourceRepo.On("CreateLink", mock.MatchedBy(func(link interface{}) bool {
				_, ok := link.(*url.URL)
				return ok
			}), mock.AnythingOfType("string")).
				Return(resultLink, true, it.expectedError)

			cachedRepo := NewCachedLinksRepository(mockSourceRepo, expiration, cleanupInterval)

			//Act
			linkModel, isNew, err := cachedRepo.CreateLink(it.targetUrl, it.urlName)

			//Assert
			if it.expectedError != nil {
				assert.EqualError(t, err, it.expectedError.Error())
			} else {
				assert.NoError(t, err)
			}

			assert.Equal(t, *it.targetUrl, linkModel.TargetUrl)
			assert.Equal(t, it.urlName, linkModel.Name)
			assert.True(t, isNew)

			cachedTargetUrl, ok := cachedRepo.GetUrl(linkModel.Hash)

			if it.expectedError != nil {
				assert.False(t, ok)
				assert.Empty(t, cachedTargetUrl)
			} else {
				assert.True(t, ok, "expected %v to be cached, but got %v, ok=%q", it.targetUrl.String(), cachedTargetUrl, ok)
				assert.Equal(t, it.targetUrl.String(), cachedTargetUrl, "expected %v to be cached, but got %v", it.targetUrl.String(), cachedTargetUrl)
			}

			if it.expireAfter != -1 {
				time.Sleep(it.expireAfter)

				cachedTargetUrl, ok = cachedRepo.GetUrl(linkModel.Hash)
				assert.False(t, ok)
				assert.Empty(t, cachedTargetUrl)
			}
		})
	}
}

func TestCachedLinksRepository_GetTargetUrlByHash(t *testing.T) {
	expiration, cleanupInterval := time.Duration(1*time.Second), time.Duration(2*time.Second)

	records := []string{
		"http://www.google.com", "ftp://ftp.google.com", "wss://ws.google.com",
	}
	cases := []struct {
		name string

		targetUrl  *url.URL
		cacheState []string

		expectedError error
	}{
		{
			"It should get target url from cache",
			must(url.Parse("http://www.google.com")).(*url.URL),
			records,
			nil,
		},
		{
			"It should get target url from source repo if no entry in cache",
			must(url.Parse("http://www.google.com")).(*url.URL),
			[]string{},
			nil,
		},
	}

	for _, it := range cases {
		t.Run(it.name, func(t *testing.T) {
			//Arrange
			mockSourceRepo := &mocks.ILinksRepository{}
			hash := model.NewLink(it.targetUrl, "", nil, nil).Hash

			call := mockSourceRepo.On("GetTargetUrlByHash", mock.AnythingOfType("string")).
				Return(it.targetUrl.String(), it.expectedError)

			if len(it.cacheState) != 0 {
				call.Maybe()
			}

			cachedRepo := NewCachedLinksRepository(mockSourceRepo, expiration, cleanupInterval)

			for _, targetUrl := range it.cacheState {
				cachedRepo.SetUrl(model.NewLink((must(url.Parse(targetUrl)).(*url.URL)), "", nil, nil).Hash, targetUrl)
			}

			//Act
			targetUrl, err := cachedRepo.GetTargetUrlByHash(hash)

			//Assert
			assert.NoError(t, err)
			assert.Equal(t, targetUrl, it.targetUrl.String())
			mockSourceRepo.AssertExpectations(t)

			if len(it.cacheState) == 0 {
				mockSourceRepo.AssertNumberOfCalls(t, "GetTargetUrlByHash", 1)
				mockSourceRepo.AssertCalled(t, "GetTargetUrlByHash", hash)
			} else {
				mockSourceRepo.AssertNotCalled(t, "GetTargetUrlByHash", hash)
				assert.NoError(t, err)
			}
		})
	}
}

func TestCachedLinksRepository_SetUrl(t *testing.T) {
	cases := []struct {
		name string

		targetUrl *url.URL
	}{
		{
			"It should update and prolonge expiration of already cached hash",
			must(url.Parse("http://www.example1.com")).(*url.URL),
		},
	}

	for _, it := range cases {
		t.Run(it.name, func(t *testing.T) {
			//Arrange
			mockSourceRepo := &mocks.ILinksRepository{}

			cacheExpiration, cacheCleanupInterval := time.Duration(1*time.Second), time.Duration(2*time.Second)

			cachedRepo := NewCachedLinksRepository(mockSourceRepo, cacheExpiration, cacheCleanupInterval)
			link := model.NewLink(it.targetUrl, "", nil, nil)
			cachedRepo.SetUrl(link.Hash, it.targetUrl.String())

			//Act
			time.Sleep(500 * time.Millisecond)
			cachedRepo.SetUrl(link.Hash, it.targetUrl.String())

			//Assert
			cachedUrl, ok := cachedRepo.GetUrl(link.Hash)
			assert.True(t, ok)
			assert.Equal(t, it.targetUrl.String(), cachedUrl)
		})
	}
}

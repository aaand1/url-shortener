package repository

import (
	"bluefield/url-shortener/model"
	"github.com/pmylund/go-cache"
	"net/url"
	"time"
)

var (
//logger = log.Logger.WithField("prefix", "linkscache")
)

var CachedLinkRepo ICachedLinksRepository

type ILinksCacheStorage interface {
	GetUrl(hash string) (string, bool)
	SetUrl(hash, targetUrl string)
	DeleteUrl(hash string)
}

type ICachedLinksRepository interface {
	ILinksRepository
	ILinksCacheStorage
}

type CachedLinksRepository struct {
	ILinksRepository
	cache *cache.Cache
}

func NewCachedLinksRepository(sourceRepo ILinksRepository, cacheExpiration, cacheCleanupInterval time.Duration) ICachedLinksRepository {
	return &CachedLinksRepository{
		sourceRepo,
		cache.New(cacheExpiration, cacheCleanupInterval),
	}
}

// Get returns non expired item from cache
func (r *CachedLinksRepository) GetUrl(hash string) (string, bool) {
	item, found := r.cache.Get(hash)
	if !found {
		return "", false
	}
	return item.(string), found
}

func (r *CachedLinksRepository) DeleteUrl(hash string) {
	r.cache.Delete(hash)
}

func (r *CachedLinksRepository) SetUrl(hash, targetUrl string) {
	//logger.Debugf("Adding link record to cache: hash=%q, targetUrl=%q", hash, targetUrl)
	r.cache.Set(hash, targetUrl, cache.DefaultExpiration)
}

func (r *CachedLinksRepository) CreateLink(targetUrl *url.URL, name string) (linkModel *model.Link, isNew bool, err error) {
	linkModel, isNew, err = r.ILinksRepository.CreateLink(targetUrl, name)
	if err == nil {
		r.SetUrl(linkModel.Hash, linkModel.TargetUrl.String())
	}
	return linkModel, isNew, err
}

func (r *CachedLinksRepository) GetTargetUrlByHash(hash string) (string, error) {
	if url, ok := r.GetUrl(hash); ok {
		return url, nil
	}
	return r.ILinksRepository.GetTargetUrlByHash(hash)
}

func (r *CachedLinksRepository) DeleteLink(hash string) error {
	r.DeleteUrl(hash)
	return r.ILinksRepository.DeleteLink(hash)
}

#!/bin/sh

go get ./...
#go get github.com/vektra/mockery/ .../. &&  mockery -all
go test -v -race -tags=unit ./...

# INTEGRATION TESTS
export URL_SHORTENER_SERVICEHOST=localhost
export URL_SHORTENER_SERVICEPORT=8081
export URL_SHORTENER_REDIRECTURLBASEPATH=http://localhost:8081
export URL_SHORTENER_CONNECTIONSTRING="host=localhost user=root password=toor dbname=url_shortener"
go run main.go &
echo $! >./pid.file
export URL_SHORTENER_INTEGRATION_TESTS_SERVICE_BASE_URL=http://localhost:8081/
go test -v -tags=integration -race ./...
kill $(cat ./pid.file)
if [$? -ne 0]; then
    echo "INTEGRATION TESTS FAILED"
    exit 1
fi


#URL_SHORTENER_SERVICEHOST=localhost
#URL_SHORTENER_SERVICEPORT=8080
export URL_SHORTENER_REDIRECTURLBASEPATH=http://localhost:8080/api
export URL_SHORTENER_DATA_SOURCE_TYPE=sqlite
export URL_SHORTENER_CONNECTIONSTRING=./url-shortener.db
go run main.go | tee run_$(date +%s).log